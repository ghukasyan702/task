<?php

namespace Modules\Task\Services;

use Modules\Task\Models\Task;

interface TaskServiceInterface
{
    public function getAll();

    public function store(array $data);

    public function getById($id);

    public function update(Task $task, array $data);

    public function destroy($id);

}
