<?php

namespace Modules\Task\Services;

use Modules\Task\Models\Task;

class TaskService implements TaskServiceInterface
{

    public function getAll()
    {
        return Task::with(['user'])
            ->orderByDesc('id')
            ->get();
    }

    public function store(array $data)
    {
        return Task::create($data)->load(['user']);
    }

    public function getById($id)
    {
        // TODO: Implement getById() method.
    }

    public function update(Task $task, array $data)
    {
        return tap($task)->update($data)->load(['user']);
    }

    public function destroy($id)
    {
        // TODO: Implement destroy() method.
    }
}
