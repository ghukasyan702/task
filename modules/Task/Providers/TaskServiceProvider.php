<?php

namespace Modules\Task\Providers;

use Illuminate\Support\ServiceProvider;
use Modules\Task\Models\Task;
use Modules\Task\Observers\TaskObserver;
use Modules\Task\Services\TaskService;
use Modules\Task\Services\TaskServiceInterface;

class TaskServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind(TaskServiceInterface::class, TaskService::class);
    }

    public function boot(): void
    {
        $this->loadMigrationsFrom(__DIR__ . '/../Database/Migrations');
        $this->app->register(TaskRouteServiceProvider::class);

        Task::observe(TaskObserver::class);
    }
}
