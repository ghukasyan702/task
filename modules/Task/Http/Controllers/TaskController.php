<?php

namespace Modules\Task\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Modules\Task\Http\Requests\TaskStoreRequest;
use Modules\Task\Http\Requests\TaskUpdateRequest;
use Modules\Task\Http\Resources\TaskResource;
use Modules\Task\Models\Task;
use Modules\Task\Services\TaskServiceInterface;

class TaskController extends Controller
{
    private $taskService;

    public function __construct(TaskServiceInterface $taskService)
    {
        $this->taskService = $taskService;
    }


    /**
     * @OA\Get(
     *     path="/api/v1/tasks",
     *     summary="Получение списка задач",
     *     tags={"Task"},
     *
     *     @OA\Response(
     *          response=200,
     *          description="Успешно создан",
     *          @OA\JsonContent(
     *              type="array",
     *              @OA\Items(
     *                  ref="#/components/schemas/TaskResponse"
     *              ),
     *          ),
     *     ),
     * )
     */
    public function index(Request $request)
    {
        return TaskResource::collection($this->taskService->getAll())->resolve();
    }


    /**
     * @OA\Get(
     *     path="/api/v1/tasks/{id}",
     *     summary="Получение задачи по его id",
     *     tags={"Task"},
     *
     *     @OA\Parameter(
     *          name="id",
     *          in="path",
     *          required=true,
     *          @OA\Schema(type="integer")
     *     ),
     *
     *     @OA\Response(
     *          response=200,
     *          description="Получение задачи по его id (status может быть 1 или 0)",
     *          @OA\JsonContent(
     *               ref="#/components/schemas/TaskResponse"
     *          )
     *     ),
     * )
     */
    public function show(Task $task)
    {
        $task->load(['user']);

        return TaskResource::make($task)->resolve();
    }



    /**
     * @OA\Post(
     *     path="/api/v1/tasks",
     *     summary="создание задачи",
     *     tags={"Task"},
     *
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\JsonContent(
     *             ref="#/components/schemas/TaskStoreRequest"
     *         )
     *     ),
     *
     *     @OA\Response(
     *          response=201,
     *          description="Успешно создан",
     *          @OA\JsonContent(
     *              ref="#/components/schemas/TaskResponse"
     *          )
     *     ),
     * )
     */
    public function store(TaskStoreRequest $request)
    {
        $data = $request->validated();

        return TaskResource::make($this->taskService->store($data))->resolve();
    }


    /**
     * @OA\Put(
     *     path="/api/v1/tasks/{id}",
     *     summary="Обновление задачи",
     *     tags={"Task"},
     *
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\JsonContent(
     *             allOf={
     *                 @OA\Schema(
     *                     ref="#/components/schemas/TaskStoreRequest"
     *                 ),
     *                 @OA\Schema(
     *                     @OA\Property(property="status", type="integer", example="1")
     *                 )
     *
     *             }
     *         )
     *     ),
     *
     *     @OA\Response(
     *          response=200,
     *          description="Успешно обновлен",
     *          @OA\JsonContent(
     *               ref="#/components/schemas/TaskResponse"
     *          )
     *     ),
     * )
     */
    public function update(TaskUpdateRequest $request, Task $task)
    {
        $data = $request->validated();
        return TaskResource::make($this->taskService->update($task,$data))->resolve();
    }

    /**
     * @OA\Delete(
     *     path="/api/v1/tasks/{id}",
     *     summary="Удаление пользователя",
     *     tags={"Task"},
     *
     *     @OA\Parameter(
     *          name="id",
     *          in="path",
     *          required=true,
     *          @OA\Schema(type="integer")
     *      ),
     *
     *     @OA\Response(
     *          response=200,
     *          description="Успешно удален",
     *     ),
     * )
     */
    public function destroy(Task $task)
    {
        $task->delete();
    }
}
