<?php

namespace Modules\Task\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Modules\User\Http\Resources\UserResource;

class TaskResource extends JsonResource
{
    /**
     * @OA\Schema(
     *      schema="TaskResponse",
     *      @OA\Property(property="id", type="integer", example="id"),
     *      @OA\Property(property="user", type="object",
     *          @OA\Property(property="id", type="integer", example=1),
     *          @OA\Property(property="name", type="string", example="Test User"),
     *      ),
     *      @OA\Property(property="title", type="string", example="Title"),
     *      @OA\Property(property="description", type="string", example="description"),
     *      @OA\Property(property="satus", type="integer", example=0),
     *  )
     */
    public function toArray(Request $request): array
    {
        return [
            'id' => $this->id,
            'user' => UserResource::make($this->whenLoaded('user')),
            'title' => $this->title,
            'description' => $this->description,
            'status' => $this->status,
            'created_at' => $this->created_at,
        ];
    }
}
