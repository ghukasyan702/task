<?php

namespace Modules\Task\Observers;



use Illuminate\Support\Facades\Log;
use Modules\Task\Models\Task;

class TaskObserver
{
    /**
     * Handle the Task "created" event.
     */
    public function created(Task $task): void
    {

    }

    /**
     * Handle the Task "updated" event.
     */
    public function updated(Task $task): void
    {
        if ($task->isDirty('status')){
            $oldStatus = $task->getOriginal('status');
            $newStatus = $task->status;

            Log::info("Статус задачи с ID {$task->id} изменен с {$oldStatus} на {$newStatus}");
        }
    }

    /**
     * Handle the Task "deleted" event.
     */
    public function deleted(Task $task): void
    {
        //
    }

    /**
     * Handle the Task "restored" event.
     */
    public function restored(Task $task): void
    {
        //
    }

    /**
     * Handle the Task "force deleted" event.
     */
    public function forceDeleted(Task $task): void
    {
        //
    }
}
