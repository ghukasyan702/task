<?php

namespace Modules\Task\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Modules\User\Models\User;


/**
 * @OA\Schema(
 *       schema="TaskStoreRequest",
 *       @OA\Property(property="title", type="string", example="Title"),
 *       @OA\Property(property="description", type="string", example="description"),
 *       @OA\Property(property="user_id", type="integer", example=1),
 * )
 */
class Task extends Model
{
    use HasFactory;

    protected $fillable = [
      'user_id',
      'title',
      'description',
      'status'
    ];

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }
}
