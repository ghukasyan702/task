<?php

namespace Modules\User\Services;

use Modules\User\Models\User;

class UserService implements UserServiceInterface
{

    public function getAll()
    {
        return User::with(['tasks'])
            ->orderByDesc('id')
            ->get();
    }

    public function store(array $data)
    {
        return User::create($data)->load(['tasks']);
    }

    public function getById($id)
    {
        // TODO: Implement getById() method.
    }

    public function update(User $user, array $data)
    {
        return tap($user)->update($data)->load(['tasks']);
    }

    public function destroy($id)
    {
        // TODO: Implement destroy() method.
    }
}
