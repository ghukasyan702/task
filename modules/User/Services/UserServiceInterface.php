<?php

namespace Modules\User\Services;

use Modules\User\Models\User;

interface UserServiceInterface
{
    public function getAll();

    public function store(array $data);

    public function getById($id);

    public function update(User $user, array $data);

    public function destroy($id);

}
