<?php

namespace Modules\User\Providers;

use Illuminate\Foundation\Support\Providers\RouteServiceProvider;
use Illuminate\Support\Facades\Route;

class UserRouteServiceProvider extends RouteServiceProvider
{
    public function boot(): void
    {

        $this->routes(function () {
            Route::middleware('api')
                ->prefix('api/v1')
                ->group(__DIR__ .'/../routes/api.php');

            Route::middleware('web')
                ->group(base_path('routes/web.php'));
        });

    }

}
