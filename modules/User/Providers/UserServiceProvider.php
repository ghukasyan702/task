<?php

namespace Modules\User\Providers;

use Illuminate\Support\ServiceProvider;
use Modules\User\Services\UserService;
use Modules\User\Services\UserServiceInterface;

class UserServiceProvider extends ServiceProvider
{

    public function register()
    {
        $this->app->bind(UserServiceInterface::class, UserService::class);
    }

    public function boot(): void
    {
        $this->loadMigrationsFrom(__DIR__ . '/../Database/Migrations');
        $this->app->register(UserRouteServiceProvider::class);
    }
}
