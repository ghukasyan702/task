<?php

namespace Modules\User\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Modules\Task\Http\Resources\TaskResource;

class UserResource extends JsonResource
{
    /**
     * @OA\Schema(
     *      schema="UserResponse",
     *      @OA\Property(property="id", type="integer", example="id"),
     *      @OA\Property(property="name", type="string", example="Test"),
     *      @OA\Property(property="email", type="string", example="test@test.com"),
     *      @OA\Property(property="tasks", type="array",
     *          @OA\Items(
     *              allOf={
     *                  @OA\Schema(
     *                      @OA\Property(property="id", type="integer" , example=1),
     *                      @OA\Property(property="title", type="string" , example="Title"),
     *                      @OA\Property(property="description", type="string" , example="Description"),
     *                      @OA\Property(property="status", type="boolean" , example=1 , description="может быть 1 или 0"),
     *                  ),
     *              }
     *          )
     *      ),
     *  )
     */
    public function toArray(Request $request): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'email' => $this->when($request->routeIs('users.*'),$this->email),
            'tasks' => TaskResource::collection($this->whenLoaded('tasks')),
            'created_at' => $this->when($request->routeIs('users.*'),$this->created_at),
        ];
    }
}
