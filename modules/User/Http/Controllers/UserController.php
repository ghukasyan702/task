<?php

namespace Modules\User\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Modules\User\Http\Requests\UserStoreRequest;
use Modules\User\Http\Requests\UserUpdateRequest;
use Modules\User\Http\Resources\UserResource;
use Modules\User\Models\User;
use Modules\User\Services\UserServiceInterface;


/**
 * @OA\Info(
 *     version="1.0",
 *     description="Задачи",
 *     title="Задачи"
 * )
 *
 * @OA\PathItem(
 *     path="/api/"
 * )
 */
class UserController extends Controller
{
    private $userService;

    public function __construct(UserServiceInterface $userService)
    {
        $this->userService = $userService;
    }


    /**
     * @OA\Get(
     *     path="/api/v1/users",
     *     summary="Получение списка пользователей",
     *     tags={"User"},
     *
     *     @OA\Response(
     *          response=200,
     *          description="Успешно создан",
     *          @OA\JsonContent(
     *              type="array",
     *              @OA\Items(
     *                  allOf={
     *                      @OA\Schema(
     *                          ref="#/components/schemas/UserResponse"
     *                      ),
     *                  }
     *              ),
     *          ),
     *     ),
     * )
     */
    public function index(Request $request)
    {
        return UserResource::collection($this->userService->getAll())->resolve();
    }



    /**
     * @OA\Get(
     *     path="/api/v1/users/{id}",
     *     summary="Получение пользователя по его id",
     *     tags={"User"},
     *
     *     @OA\Parameter(
     *          name="id",
     *          in="path",
     *          required=true,
     *          @OA\Schema(type="integer")
     *     ),
     *
     *     @OA\Response(
     *          response=200,
     *          description="Получение пользователя по его id",
     *          @OA\JsonContent(
     *               ref="#/components/schemas/UserResponse"
     *          )
     *     ),
     * )
     */
    public function show(User $user)
    {
        $user->load(['tasks']);
        return UserResource::make($user)->resolve();
    }

    /**
     * @OA\Post(
     *     path="/api/v1/users",
     *     summary="создание пользователя",
     *     tags={"User"},
     *
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\JsonContent(
     *             ref="#/components/schemas/UserStoreRequest"
     *         )
     *     ),
     *
     *     @OA\Response(
     *          response=201,
     *          description="Успешно создан",
     *          @OA\JsonContent(
     *              ref="#/components/schemas/UserResponse"
     *          )
     *     ),
     * )
     */
    public function store(UserStoreRequest $request)
    {
        $data = $request->validated();
        return UserResource::make($this->userService->store($data))->resolve();
    }


    /**
     * @OA\Put(
     *     path="/api/v1/users/{id}",
     *     summary="Обновление пользователя",
     *     tags={"User"},
     *
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\JsonContent(
     *             ref="#/components/schemas/UserUpdateRequest"
     *         )
     *     ),
     *
     *     @OA\Response(
     *          response=200,
     *          description="Успешно обновлен",
     *          @OA\JsonContent(
     *              allOf={
     *                  @OA\Schema(
     *                      ref="#/components/schemas/UserResponse"
     *                  ),
     *              }
     *          )
     *     ),
     * )
     */
    public function update(UserUpdateRequest $request, User $user)
    {
        $data = $request->validated();
        return UserResource::make($this->userService->update($user,$data))->resolve();
    }



    /**
     * @OA\Delete(
     *     path="/api/v1/users/{id}",
     *     summary="Удаление пользователя",
     *     tags={"User"},
     *
     *     @OA\Parameter(
     *          name="id",
     *          in="path",
     *          required=true,
     *          @OA\Schema(type="integer")
     *      ),
     *
     *     @OA\Response(
     *          response=200,
     *          description="Успешно удален",
     *     ),
     * )
     */
    public function destroy(User $user)
    {
        $user->delete();
    }
}
